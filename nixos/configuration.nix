# This is your system's configuration file.
# Use this to configure your system environment (it replaces /etc/nixos/configuration.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: {
  # You can import other NixOS modules here
  imports = [
    # If you want to use modules from other flakes (such as nixos-hardware):
    # inputs.hardware.nixosModules.common-cpu-amd
    # inputs.hardware.nixosModules.common-ssd

    # You can also split up your configuration and import pieces of it here:
    # ./users.nix

    inputs.home-manager.nixosModules.home-manager
  ];

  nixpkgs = {
    # Configure your nixpkgs instance
    config = {
      # Disable if you don't want unfree packages
      allowUnfree = true;
    };
  };

  # This will add each flake input as a registry
  # To make nix3 commands consistent with your flake
  nix.registry = (lib.mapAttrs (_: flake: {inherit flake;})) ((lib.filterAttrs (_: lib.isType "flake")) inputs);

  # This will additionally add your inputs to the system's legacy channels
  # Making legacy nix commands consistent as well, awesome!
  nix.nixPath = ["/etc/nix/path"];
  environment.etc =
    lib.mapAttrs'
    (name: value: {
      name = "nix/path/${name}";
      value.source = value.flake;
    })
    config.nix.registry;

  nix.settings = {
    # Enable flakes and new 'nix' command
    experimental-features = "nix-command flakes";
    # Deduplicate and optimize nix store
    auto-optimise-store = true;
  };

  home-manager = {
    extraSpecialArgs = { inherit inputs outputs; };
    users = {
      # Import your home-manager configuration
      libredove = import ../home-manager/home.nix;
    };
  };

  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };

  # OpenGL
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver
      vaapiIntel
      vaapiVdpau
      libvdpau-va-gl
    ];
    driSupport = true;
  };

  # FIXME: Add the rest of your current configuration
  networking.networkmanager.enable = true;
  time.timeZone = "Europe/Budapest";
  i18n.defaultLocale = "C.UTF-8";
  services.xserver.enable = true;
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
  services.xserver.desktopManager.xterm.enable = false;
# services.gnome.core-utilities.enable = false;
  services.xserver = {
    layout = "gb";
    xkbVariant = "";
  };
  console.keyMap = "uk";
  services.printing.enable = true;
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
  };
  services.flatpak.enable = true;
  services.fwupd.enable = true;
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
  };
  virtualisation.libvirtd.enable = true;
  virtualisation.libvirtd.qemu.ovmf.packages = [ pkgs.OVMFFull.fd ];
  programs.virt-manager.enable = true;
  documentation.man.mandoc.enable = true;
  documentation.man.man-db.enable = false;
  environment.systemPackages = with pkgs; [
    vim wget curl git ripgrep gnumake # gnome-console gnome.nautilus loupe
  ];
  environment.gnome.excludePackages = (with pkgs; [
    gnome-photos
    gnome-tour
    snapshot
  ]) ++ (with pkgs.gnome; [
    gnome-calculator
    gnome-calendar
    gnome-clocks
    gnome-contacts
    gnome-maps
    gnome-weather
    gnome-music
    gnome-terminal
    epiphany # web browser
    geary # email reader
    evince # document viewer
    gnome-contacts
    geary
    totem # video player
  ]);  
  services.xserver.excludePackages = [ 
    pkgs.xorg.xorgserver
    pkgs.xorg.xrandr
    pkgs.xorg.xrdb
    pkgs.xorg.setxkbmap
    pkgs.xorg.iceauth # required for KDE applications (it's called by dcopserver)
    pkgs.xorg.xlsclients
    pkgs.xorg.xset
    pkgs.xorg.xsetroot
    pkgs.xorg.xinput
    pkgs.xorg.xprop
    pkgs.xorg.xauth
    pkgs.xterm
    pkgs.xorg.xf86inputevdev.out # get evdev.4 man page
  ];
  users.users.libredove = {
    isNormalUser = true;
    description = "Michael Libredove";
    extraGroups = [ "networkmanager" "wheel" "libvirtd" "kvm" ];
    hashedPassword = "$y$j9T$eeUonJF2tcJ4pV9flO/cl1$GVL2f5AB59oxIEJgyKqBWAIkWCa1Pew78nKi4kfxtm9";
  };

  # TODO: This is just an example, be sure to use whatever bootloader you prefer
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.systemd.enable = true;
  boot.plymouth.enable = true;
  zramSwap.enable = true;
 
  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  system.stateVersion = "23.11";
}
