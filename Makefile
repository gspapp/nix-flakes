systemboot:
	sudo nixos-rebuild boot --flake .

systemswitch:
	sudo nixos-rebuild switch --flake .

pull:
	git pull

update:
	sudo nix flake update

upgrade: update systemboot

gc:
	sudo nix-collect-garbage -d
	nix store gc --debug
